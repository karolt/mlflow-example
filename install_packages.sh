sudo apt-get update
sudo apt-get install -y byobu jq curl wget
wget https://repo.anaconda.com/miniconda/Miniconda3-py37_4.10.3-Linux-x86_64.sh
bash Miniconda3-py37_4.10.3-Linux-x86_64.sh # all yes
# restart terminal
./miniconda3/bin/conda install -c conda-forge jupyterlab seaborn pandas mlflow pandas-profiling ipywidgets scikit-learn
